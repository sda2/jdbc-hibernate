package com.example;

import java.sql.*;
import java.util.Scanner;

public class GetUserDetails {

    private static String sql;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        //read user entered data
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter email id:");
        String id = scanner.nextLine();
        System.out.println("User id=" + id);
        System.out.println("Please enter lastname to get details:");
        String lastName = scanner.nextLine();
        System.out.println("User lastname=" + lastName);
        printUserData(id, lastName);

    }
   //david@gmail.com or \'1\'=\\'1///5***^& AND DROP TABLE users' david@gmail.com or '1\'=\\'1///5***^& AND DROP TABLE users'


    private static void insert(String id, String lastName) throws ClassNotFoundException, SQLException {

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = DBConnection.getConnection();
            stmt = con.createStatement();
            sql = "INSERT user(last_name, email) VALUES " +
                    "('" + lastName + "', '" + id + "')";

            System.out.println(sql);
            int ret = stmt.executeUpdate(sql);

            printUserData(id, lastName);

        } finally {
            if (rs != null)
                rs.close();
            stmt.close();
            con.close();
        }

    }

    private static void printUserData(String id, String lastName) throws ClassNotFoundException, SQLException {

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = DBConnection.getConnection();
            stmt = con.createStatement();
            String query = "select first_name,last_name, country from user where email = '" + id + "' and last_name='" + lastName + "'";
            System.out.println(query);
            rs = stmt.executeQuery(query);

            boolean resultExists = false;
            while (rs.next()) {
                resultExists = true;
                System.out.println("FirstName=" + rs.getString("first_name") + ",country=" + rs.getString("country") + ",lastName=" + rs.getString("last_name"));
            }

            if (!resultExists) {
                System.out.println("Unable to retrieve result in the database");
            }
        } finally {
            if (rs != null)
                rs.close();
            stmt.close();
            con.close();
        }

    }

}
