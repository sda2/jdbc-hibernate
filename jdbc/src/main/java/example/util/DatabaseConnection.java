package example.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Connection connection;

    private DatabaseConnection() {
    }

    public static Connection getConnection() throws SQLException {
        if (connection != null) {
            return connection;
        }

        final String url = "jdbc:mysql://localhost:3306/sda_jdbc_db";
        final String user = "root";
        final String pass = "password";

        connection = DriverManager.getConnection(url, user, pass);

        if (connection != null) {return connection;}
        throw new SQLException("Unable to get connection");
    }
}
