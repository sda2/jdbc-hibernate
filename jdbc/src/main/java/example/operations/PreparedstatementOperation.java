package example.operations;

import example.util.DatabaseConnection;

import java.sql.*;

public class PreparedstatementOperation {

    private Connection conn = null;
    private static final String SELECT = "SELECT id, first_name, last_name, email, country FROM user WHERE first_name = ?";
    private static final String INSERT = "INSERT user(first_name,last_name, email, country) VALUES (?, ?,?, ?)";
    private static final String UPDATE = "UPDATE user SET first_name = ?,last_name=?, email = ?, country = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM user WHERE id = ?";

    public PreparedstatementOperation() {
        try {
            conn = DatabaseConnection.getConnection();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private int selectOperation(String userFirstName) {
        try {
            if (conn != null) {
                //SELECT
                PreparedStatement pstmt = conn.prepareStatement(SELECT);
                pstmt.setString(1, userFirstName);
                ResultSet rs = pstmt.executeQuery();
                boolean next = rs.next();
                if (next) {
                    int id = rs.getInt("id");
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String email = rs.getString("email");
                    String country = rs.getString("country");
                    System.out.println(id + ", " + firstName + ", " + lastName + ", " + email + ", " + country);
                    return id;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    private void insertOperation(String firstName, String lastName, String email, String country) {
        try {
            if (conn != null) {
                //INSERT
                PreparedStatement pstmt = conn.prepareStatement(INSERT);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, country);
                int ret = pstmt.executeUpdate();
                System.out.println("Insert return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void updateOperation(int id, String firstName, String lastName, String email, String country) {
        try {
            if (conn != null) {
                //UPDATE
                PreparedStatement pstmt = conn.prepareStatement(UPDATE);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, country);
                pstmt.setInt(5, id);
                int ret = pstmt.executeUpdate();
                System.out.println("Update return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void deleteOperation(int id) {
        try {
            if (conn != null) {
                //DELETE
                PreparedStatement pstmt = conn.prepareStatement(DELETE);
                pstmt.setInt(1, id);
                int ret = pstmt.executeUpdate();
                System.out.println("Delete return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void preparedstatementOperation() {
        PreparedstatementOperation command = new PreparedstatementOperation();
        command.insertOperation("test1", "Sample_lastName", "test1@test.com", "Poland");
        int id = command.selectOperation("test1");
        command.updateOperation(id, "test2", "Sample_lastName2", "test2@test.com", "Poland");
        id = command.selectOperation("test2");
        command.deleteOperation(id);
    }
}
