package example.operations;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static example.util.DatabaseConnection.getConnection;

public class CommandOperation {

    private Statement stmt = null;

    public CommandOperation() {
        try {
            stmt = getConnection().createStatement();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private int selectOperation(String userFirstName) {
        try {
            if (stmt != null) {
                //SELECT
                ResultSet rs = stmt.executeQuery("SELECT id, first_name,last_name, email, country " +
                        "FROM user WHERE first_name = '" + userFirstName + "'");
                boolean next = rs.next();
                if (next) {
                    int id = rs.getInt("id");
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String email = rs.getString("email");
                    String country = rs.getString("country");
                    System.out.println(id + ", " + firstName + ", " + lastName + ", " + email + ", " + country);
                    return id;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    private void insertOperation(String firstName, String lastName, String email, String country) {
        try {
            if (stmt != null) {
                //INSERT
                int ret = stmt.executeUpdate("INSERT user(first_name,last_name, email, country) VALUES " +
                        "('" + firstName + "', '" + lastName + "', '" + email + "', '" + country + "')");
                System.out.println("Insert return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void updateOperation(int id, String firstName, String lastName, String email, String country) {
        try {
            if (stmt != null) {
                //UPDATE
                int ret = stmt.executeUpdate("UPDATE user SET first_name = '" + firstName + "', last_name='" + lastName + "', " +
                        "email = '" + email + "', country = '" + country + "' WHERE id = " + id);
                System.out.println("Update return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void deleteOperation(int id) {
        try {
            if (stmt != null) {
                //DELETE
                int ret = stmt.executeUpdate("DELETE FROM user WHERE id = " + id);
                System.out.println("Delete return: " + (ret == 1 ? "OK" : "ERROR"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void commandOperation() {
        CommandOperation command = new CommandOperation();
        command.insertOperation("test", "lastName", "test1@test.com", "Poland");

        int id = command.selectOperation("test");

        command.updateOperation(id, "test4", "lastName", "test2@test.com", "Poland");

        id = command.selectOperation("test4");

       // command.deleteOperation(id);
    }
}
