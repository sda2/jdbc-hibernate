package example.operations;

import java.sql.*;

import static example.util.DatabaseConnection.getConnection;

public class TransactionOperation {
    private Connection conn = null;
    private static final String SELECT = "SELECT id, first_name,last_name, email, country FROM user WHERE first_name = ?";
    private static final String INSERT = "INSERT user(first_name,last_name, email, country) VALUES (?, ?, ?,?)";
    private static final String UPDATE = "UPDATE user SET first_name = ?, last_name=?, email = ?, country = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM user WHERE id = ?";

    public TransactionOperation() {
        try {
            conn = getConnection();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            //use a logger
            ex.printStackTrace();
        }
    }

    private void setAutoCommit(boolean isAutoCommit) {
        if (conn != null) {
            try {
                conn.setAutoCommit(isAutoCommit);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void commit() {
        if (conn != null) {
            try {
                conn.commit();
                System.out.println("COMMIT");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void rollback() {
        if (conn != null) {
            try {
                conn.rollback();
                System.out.println("ROLLBACK");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private int selectOperation(String userFirstName) {
        try {
            if (conn != null) {
                //SELECT
                PreparedStatement pstmt = conn.prepareStatement(SELECT);
                pstmt.setString(1, userFirstName);
                ResultSet rs = pstmt.executeQuery();
                boolean next = rs.next();
                if (next) {
                    int id = rs.getInt("id");
                    String firstName = rs.getString("first_name");
                    String lastName = rs.getString("last_name");
                    String email = rs.getString("email");
                    String country = rs.getString("country");
                    System.out.println(id + ", " + firstName + ", " + lastName + ", " + email + ", " + country);
                    return id;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    private boolean insertOperation(String firstName, String lastName, String email, String country) {
        try {
            if (conn != null) {
                //INSERT
                PreparedStatement pstmt = conn.prepareStatement(INSERT);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, country);
                int ret = pstmt.executeUpdate();
                System.out.println("Insert return: " + (ret == 1 ? "OK" : "ERROR"));
                return ret == 1;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private boolean updateOperation(int id, String firstName, String lastName, String email, String country) {
        try {
            if (conn != null) {
                //UPDATE
                PreparedStatement pstmt = conn.prepareStatement(UPDATE);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, country);
                pstmt.setInt(5, id);
                int ret = pstmt.executeUpdate();
                System.out.println("Update return: " + (ret == 1 ? "OK" : "ERROR"));
                return ret == 1;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private boolean deleteOperation(int id) {
        try {
            if (conn != null) {
                //DELETE
                PreparedStatement pstmt = conn.prepareStatement(DELETE);
                pstmt.setInt(1, id);
                int ret = pstmt.executeUpdate();
                System.out.println("Delete return: " + (ret == 1 ? "OK" : "ERROR"));
                return ret == 1;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void transactionOperation() {
        TransactionOperation command = new TransactionOperation();
        command.setAutoCommit(false);
        boolean status = command.insertOperation("test1", "lastname", "test1@test.com", "Poland");
        if (status) {
            int id = command.selectOperation("test1");
            status = command.updateOperation(id, "test2", "lastname", "test2@test.com", "Poland");
        }
        if (status) {
            int id = command.selectOperation("test2");
            status = command.deleteOperation(id);
        }
        if (status) {
            command.commit();
        } else {
            command.rollback();
        }
        int id = command.selectOperation("test2");
        command.setAutoCommit(true);
    }
}
