package example.operations;

import java.sql.*;

import static example.util.DatabaseConnection.getConnection;

public class QueryOperation {

    public static void queryOperation() {
        try {
            Connection conn = getConnection();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, first_name,last_name, email, country FROM user");
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String email = rs.getString("email");
                String country = rs.getString("country");
                System.out.println(id + ", " + firstName + ", " + lastName + ", " + email + ", " + country);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
